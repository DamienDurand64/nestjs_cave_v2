import { ApiProperty } from '@nestjs/swagger';
import { Wine } from '@prisma/client';
import { UserEntity } from 'src/users/entities/user.entity';
import { WineColorEntity } from 'src/wine-color/entities/wineColor.entity';

export class WineEntity implements Wine {
  constructor(partial: Partial<WineEntity>) {
    Object.assign(this, partial);
  }
  @ApiProperty()
  id: string;

  @ApiProperty()
  name: string;

  @ApiProperty()
  producer: string;

  @ApiProperty()
  varietal: string[];

  @ApiProperty()
  country: string;

  @ApiProperty()
  region: string;

  @ApiProperty()
  vintage: number;

  @ApiProperty()
  purchasedAt: Date;

  @ApiProperty()
  consumedAt: Date;

  @ApiProperty()
  consumed: boolean;

  @ApiProperty()
  description: string;

  @ApiProperty()
  image: string;

  @ApiProperty()
  servingTemperature: string;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;

  @ApiProperty()
  ownerId: string;

  @ApiProperty()
  wineColorId: string;

  @ApiProperty()
  Owner: UserEntity;

  @ApiProperty()
  WineColor: WineColorEntity;

  @ApiProperty()
  WineBottle: [];
  //   WineBottle: WineBottle[];

  @ApiProperty()
  TastingNote: [];
  //   TastingNote: TastingNote[];

  @ApiProperty()
  WineDrunk: [];
  //   WineDrunk: WineDrunk[];
}
