import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateWineDto } from './dto/create-wine.dto';
import { UpdateWineDto } from './dto/update-wine.dto';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class WinesService {
  constructor(private readonly prisma: PrismaService) {}

  async create(createWineDto: CreateWineDto) {
    try {
      const { wineBottleIds, wineColorId, ownerId, ...wineData } =
        createWineDto;

      const data = {
        ...wineData,
        WineBottle: {
          connect: wineBottleIds.map((id) => ({ id })),
        },
        WineColor: {
          connect: { id: wineColorId },
        },
        Owner: {
          connect: { id: ownerId },
        },
      };

      return await this.prisma.wine.create({
        data,
      });
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  async findAllByOwnerId(ownerId: string) {
    try {
      return await this.prisma.wine.findMany({
        where: { ownerId },
        include: {
          WineBottle: true,
          WineColor: true,
        },
      });
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  async findOne(id: string, ownerId: string) {
    try {
      const wine = await this.prisma.wine.findUnique({
        where: { id, ownerId },
        include: {
          WineBottle: true,
          WineColor: true,
          Owner: true,
        },
      });

      if (!wine) {
        throw new NotFoundException('Wine not found or not owned by the user');
      }

      return wine;
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  async update(id: string, ownerId: string, updateWineDto: UpdateWineDto) {
    try {
      return await this.prisma.wine.update({
        where: { id, ownerId },
        data: { ...updateWineDto },
      });
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  async remove(id: string, ownerId: string) {
    try {
      const wine = await this.prisma.wine.findUnique({
        where: { id, ownerId },
      });

      if (!wine) {
        throw new NotFoundException('Wine not found or not owned by the user');
      }

      return await this.prisma.wine.delete({
        where: { id },
      });
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }
}
