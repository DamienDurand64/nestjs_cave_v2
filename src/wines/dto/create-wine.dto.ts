import { ApiProperty } from '@nestjs/swagger';
import {
  ArrayMaxSize,
  IsArray,
  IsBoolean,
  IsDate,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  Max,
  MaxLength,
  Min,
} from 'class-validator';

export class CreateWineDto {
  @IsString()
  @IsNotEmpty()
  @MaxLength(150)
  @ApiProperty()
  name: string;

  @IsString()
  @IsNotEmpty()
  @MaxLength(150)
  @ApiProperty()
  producer: string;

  @IsArray()
  @ArrayMaxSize(20)
  @IsString({ each: true })
  @MaxLength(50, { each: true })
  @IsNotEmpty()
  @ApiProperty()
  varietal: string[];

  @IsString()
  @IsNotEmpty()
  @MaxLength(150)
  @ApiProperty()
  country: string;

  @IsString()
  @IsNotEmpty()
  @MaxLength(150)
  @ApiProperty()
  region: string;

  @IsNumber()
  @IsNotEmpty()
  @MaxLength(4)
  @Min(1900)
  @Max(new Date().getFullYear() + 2)
  @ApiProperty()
  vintage: number;

  @IsDate()
  @IsOptional()
  @ApiProperty()
  purchasedAt: Date;

  @IsDate()
  @IsOptional()
  @ApiProperty()
  consumedAt: Date;

  @IsBoolean()
  @IsOptional()
  @ApiProperty()
  consumed: boolean;

  @IsString()
  @IsOptional()
  @MaxLength(500)
  @ApiProperty()
  description: string;

  @IsString()
  @IsOptional()
  @MaxLength(500)
  @ApiProperty()
  image: string;

  @IsString()
  @IsOptional()
  @MaxLength(150)
  @ApiProperty()
  servingTemperature: string;

  @IsNumber()
  @IsNotEmpty()
  @ApiProperty()
  ownerId: string;

  @IsNumber()
  @IsNotEmpty()
  @ApiProperty()
  wineColorId: string;

  @IsArray()
  @IsOptional()
  @IsNumber({}, { each: true })
  @ApiProperty({ required: false })
  wineBottleIds?: string[];
}
