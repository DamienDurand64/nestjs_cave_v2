import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ParseIntPipe,
  UseGuards,
  Request,
} from '@nestjs/common';
import { WinesService } from './wines.service';
import { CreateWineDto } from './dto/create-wine.dto';
import { UpdateWineDto } from './dto/update-wine.dto';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { WineEntity } from './entities/wine.entity';
import { User } from '@prisma/client';
import { JwtGuard } from 'src/auth/guards/jwt.guard';

@Controller('wines')
@UseGuards(JwtGuard)
@ApiBearerAuth()
@ApiTags('Wines')
export class WinesController {
  constructor(private readonly winesService: WinesService) {}

  @Post()
  @ApiCreatedResponse({ type: WineEntity })
  async create(@Body() createWineDto: CreateWineDto) {
    return new WineEntity(await this.winesService.create(createWineDto));
  }

  @Get()
  async findAllByOwnerId(@Request() req: { user: User }) {
    return this.winesService.findAllByOwnerId(req.user.id);
  }

  @Get(':id')
  @ApiOkResponse({ type: WineEntity })
  findOne(
    @Param('id', ParseIntPipe) id: string,
    @Request() req: { user: User },
  ) {
    return this.winesService.findOne(id, req.user.id);
  }

  @Patch(':id')
  @ApiCreatedResponse({ type: WineEntity })
  update(
    @Param('id', ParseIntPipe) id: string,
    @Request() req: { user: User },
    @Body() updateWineDto: UpdateWineDto,
  ) {
    return this.winesService.update(id, req.user.id, updateWineDto);
  }

  @Delete(':id')
  @ApiOkResponse({ type: WineEntity })
  remove(
    @Param('id', ParseIntPipe) id: string,
    @Request() req: { user: User },
  ) {
    return this.winesService.remove(id, req.user.id);
  }
}
