import {
  Body,
  Controller,
  Get,
  Post,
  Request,
  Res,
  UnauthorizedException,
  UseGuards,
} from '@nestjs/common';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { LoginDto } from './dto/login.dto';
import { UsersService } from 'src/users/users.service';
import { AuthService } from './auth.service';
import { Response } from 'express';
import { User } from '@prisma/client';
import { JwtGuard } from './guards/Jwt.guard';
import { ApiOkResponse } from '@nestjs/swagger';
import { AuthEntity } from './entity/auth.entity';

@Controller('auth')
export class AuthController {
  constructor(
    private authService: AuthService,
    private userService: UsersService,
  ) {}

  @Post('register')
  async registerUser(@Body() dto: CreateUserDto) {
    return this.userService.create(dto);
  }

  @Post('login')
  @ApiOkResponse({ type: AuthEntity })
  async login(
    @Body() dto: LoginDto,
    @Res({ passthrough: true }) response: Response,
  ) {
    try {
      const { user, token } = await this.authService.login(dto);

      response.cookie('accessToken', token, {
        httpOnly: true,
      });
      return user;
    } catch (error) {
      throw new UnauthorizedException(error);
    }
  }

  @Post('logout')
  async logout(@Res({ passthrough: true }) response: Response) {
    response.clearCookie('accessToken');
    return {
      message: 'success',
    };
  }

  @UseGuards(JwtGuard)
  @Get('me')
  async me(@Request() req: { user: User }) {
    return req.user;
  }
}
