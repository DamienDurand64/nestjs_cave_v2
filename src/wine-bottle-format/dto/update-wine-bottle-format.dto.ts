import { PartialType } from '@nestjs/swagger';
import { CreateWineBottleFormatDto } from './create-wine-bottle-format.dto';

export class UpdateWineBottleFormatDto extends PartialType(CreateWineBottleFormatDto) {}
