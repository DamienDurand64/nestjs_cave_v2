import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { WineBottleFormatService } from './wine-bottle-format.service';
import { CreateWineBottleFormatDto } from './dto/create-wine-bottle-format.dto';
import { UpdateWineBottleFormatDto } from './dto/update-wine-bottle-format.dto';

@Controller('wine-bottle-format')
export class WineBottleFormatController {
  constructor(
    private readonly wineBottleFormatService: WineBottleFormatService,
  ) {}

  @Post()
  create(@Body() createWineBottleFormatDto: CreateWineBottleFormatDto) {
    return this.wineBottleFormatService.create(createWineBottleFormatDto);
  }

  @Get()
  findAll() {
    return this.wineBottleFormatService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.wineBottleFormatService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateWineBottleFormatDto: UpdateWineBottleFormatDto,
  ) {
    return this.wineBottleFormatService.update(+id, updateWineBottleFormatDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.wineBottleFormatService.remove(+id);
  }
}
