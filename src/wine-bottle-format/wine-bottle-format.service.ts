import { Injectable } from '@nestjs/common';
import { CreateWineBottleFormatDto } from './dto/create-wine-bottle-format.dto';
import { UpdateWineBottleFormatDto } from './dto/update-wine-bottle-format.dto';

@Injectable()
export class WineBottleFormatService {
  create(createWineBottleFormatDto: CreateWineBottleFormatDto) {
    return 'This action adds a new wineBottleFormat';
  }

  findAll() {
    return `This action returns all wineBottleFormat`;
  }

  findOne(id: number) {
    return `This action returns a #${id} wineBottleFormat`;
  }

  update(id: number, updateWineBottleFormatDto: UpdateWineBottleFormatDto) {
    return `This action updates a #${id} wineBottleFormat`;
  }

  remove(id: number) {
    return `This action removes a #${id} wineBottleFormat`;
  }
}
