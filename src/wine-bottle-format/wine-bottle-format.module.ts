import { Module } from '@nestjs/common';
import { WineBottleFormatService } from './wine-bottle-format.service';
import { WineBottleFormatController } from './wine-bottle-format.controller';
import { JwtService } from '@nestjs/jwt';
@Module({
  controllers: [WineBottleFormatController],
  providers: [WineBottleFormatService, JwtService],
})
export class WineBottleFormatModule {}
