import { Test, TestingModule } from '@nestjs/testing';
import { WineBottleFormatController } from '../wine-bottle-format.controller';
import { WineBottleFormatService } from '../wine-bottle-format.service';

describe('WineBottleFormatController', () => {
  let controller: WineBottleFormatController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [WineBottleFormatController],
      providers: [WineBottleFormatService],
    }).compile();

    controller = module.get<WineBottleFormatController>(
      WineBottleFormatController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
