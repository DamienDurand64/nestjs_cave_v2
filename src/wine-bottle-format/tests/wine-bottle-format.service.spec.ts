import { Test, TestingModule } from '@nestjs/testing';
import { WineBottleFormatService } from '../wine-bottle-format.service';

describe('WineBottleFormatService', () => {
  let service: WineBottleFormatService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [WineBottleFormatService],
    }).compile();

    service = module.get<WineBottleFormatService>(WineBottleFormatService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
