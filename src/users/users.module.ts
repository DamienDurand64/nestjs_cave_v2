import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UserController } from './users.controller';
import { PrismaModule } from 'src/prisma/prisma.module';
import { JwtService } from '@nestjs/jwt';

@Module({
  controllers: [UserController],
  providers: [UsersService, JwtService],
  imports: [PrismaModule],
  exports: [UsersService],
})
export class UsersModule {}
