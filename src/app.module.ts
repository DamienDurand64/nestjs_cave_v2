import { Module } from '@nestjs/common';

import { PrismaModule } from './prisma/prisma.module';
import { WineColorModule } from './wine-color/wineColor.module';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { WinesModule } from './wines/wines.module';
import { WineBottleFormatModule } from './wine-bottle-format/wine-bottle-format.module';

@Module({
  imports: [
    PrismaModule,
    WineColorModule,
    UsersModule,
    AuthModule,
    WinesModule,
    WineBottleFormatModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
