import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ParseIntPipe,
  NotFoundException,
  UseGuards,
} from '@nestjs/common';
import { WineColorService } from './wineColor.service';
import { CreateWineColorDto } from './dto/createWineColor.dto';
import { UpdateWineColorDto } from './dto/updateWineColor.dto';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { WineColorEntity } from './entities/wineColor.entity';
import { JwtGuard } from 'src/auth/guards/Jwt.guard';

@Controller('wine-color')
@ApiTags('WineColor')
export class WineColorController {
  constructor(private readonly wineColorService: WineColorService) {}

  @Post()
  @UseGuards(JwtGuard)
  @ApiBearerAuth()
  @ApiCreatedResponse({ type: WineColorEntity })
  create(@Body() createWineColorDto: CreateWineColorDto) {
    return this.wineColorService.create(createWineColorDto);
  }

  @Get()
  @UseGuards(JwtGuard)
  @ApiBearerAuth()
  @ApiOkResponse({ type: WineColorEntity })
  findAll() {
    return this.wineColorService.findAll();
  }

  @Get(':id')
  @UseGuards(JwtGuard)
  @ApiBearerAuth()
  @ApiOkResponse({ type: WineColorEntity })
  async findOne(@Param('id', ParseIntPipe) id: string) {
    const wineColor = await this.wineColorService.findOne(id);
    if (!wineColor) {
      throw new NotFoundException(`Wine color with this id:${id} not found`);
    }
    return wineColor;
  }

  @Patch(':id')
  @UseGuards(JwtGuard)
  @ApiBearerAuth()
  @ApiCreatedResponse({ type: WineColorEntity })
  update(
    @Param('id', ParseIntPipe) id: string,
    @Body() updateWineColorDto: UpdateWineColorDto,
  ) {
    return this.wineColorService.update(id, updateWineColorDto);
  }

  @Delete(':id')
  @UseGuards(JwtGuard)
  @ApiBearerAuth()
  @ApiOkResponse({ type: WineColorEntity })
  remove(@Param('id', ParseIntPipe) id: string) {
    return this.wineColorService.remove(id);
  }
}
