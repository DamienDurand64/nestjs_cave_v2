import { Module } from '@nestjs/common';
import { WineColorService } from './wineColor.service';
import { WineColorController } from './wineColor.controller';
import { PrismaModule } from 'src/prisma/prisma.module';
import { JwtService } from '@nestjs/jwt';

@Module({
  imports: [PrismaModule],
  controllers: [WineColorController],
  providers: [WineColorService, JwtService],
})
export class WineColorModule {}
