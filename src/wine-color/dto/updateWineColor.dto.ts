import { CreateWineColorDto } from './createWineColor.dto';

export class UpdateWineColorDto extends CreateWineColorDto {}
