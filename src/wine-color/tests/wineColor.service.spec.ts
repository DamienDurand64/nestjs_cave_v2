import { Test, TestingModule } from '@nestjs/testing';
import { WineColorService } from '../wineColor.service';
import { PrismaService } from '../../prisma/prisma.service';

describe('WineColorService', () => {
  let service: WineColorService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [WineColorService, PrismaService],
    }).compile();

    service = module.get<WineColorService>(WineColorService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
