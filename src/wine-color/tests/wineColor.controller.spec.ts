import { Test, TestingModule } from '@nestjs/testing';
import { WineColorController } from '../wineColor.controller';
import { WineColorService } from '../wineColor.service';
import { PrismaService } from '../../prisma/prisma.service';

describe('WineColorController', () => {
  let controller: WineColorController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [WineColorController],
      providers: [WineColorService, PrismaService],
    }).compile();

    controller = module.get<WineColorController>(WineColorController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
