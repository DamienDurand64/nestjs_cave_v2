import { ApiProperty } from '@nestjs/swagger';
import { WineColor } from '@prisma/client';

export class WineColorEntity implements WineColor {
  @ApiProperty()
  id: string;

  @ApiProperty()
  name: string;

  @ApiProperty()
  image: string;

  @ApiProperty()
  updatedAt: Date;

  @ApiProperty()
  createdAt: Date;
}
