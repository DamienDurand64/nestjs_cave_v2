import { Injectable } from '@nestjs/common';
import { CreateWineColorDto } from './dto/createWineColor.dto';
import { UpdateWineColorDto } from './dto/updateWineColor.dto';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class WineColorService {
  constructor(private readonly prisma: PrismaService) {}

  create(createWineColorDto: CreateWineColorDto) {
    return this.prisma.wineColor.create({
      data: {
        name: createWineColorDto.name,
      },
    });
  }

  findAll() {
    return this.prisma.wineColor.findMany();
  }

  findOne(id: string) {
    return this.prisma.wineColor.findUnique({
      where: { id },
    });
  }

  update(id: string, updateWineColorDto: UpdateWineColorDto) {
    return this.prisma.wineColor.update({
      where: { id },
      data: {
        name: updateWineColorDto.name,
      },
    });
  }

  remove(id: string) {
    return this.prisma.wineColor.delete({
      where: { id },
    });
  }
}
