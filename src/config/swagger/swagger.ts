import { INestApplication } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

export class SwaggerConfig {
  constructor(private readonly app: INestApplication) {}

  config = new DocumentBuilder()
    .setTitle('Ma petite Cave')
    .setDescription('Ma petite Cave API description')
    .setVersion('0.1')
    .addBearerAuth()
    .build();

  setup() {
    const document = SwaggerModule.createDocument(this.app, this.config);
    SwaggerModule.setup('api', this.app, document);
  }
}
