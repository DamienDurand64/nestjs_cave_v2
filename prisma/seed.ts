import { PrismaClient } from '@prisma/client';
import * as bcrypt from 'bcrypt';
import { users } from './mocks/users';
import { wines } from './mocks/wines';
import { wineColors } from './mocks/wineColors';
import { bottleFormats } from './mocks/bottleFormats';

// npx prisma db seed wineColors to seed wineColors
// npx prisma db seed users to seed users
// npx prisma db seed wines to seed wines
// npx prisma db seed bottleFormats to seed bottleFormats

const prisma = new PrismaClient();

const roundsOfHashing = 10;

async function seedWineColors() {
  try {
    for (const wineColor of wineColors) {
      await prisma.wineColor.upsert({
        where: { name: wineColor },
        update: {},
        create: {
          name: wineColor,
          updatedAt: new Date(),
        },
      });
      console.log(`Added wine color: ${wineColor}`);
    }
  } catch (error) {
    console.error(`Error adding wine color:`, error);
  }
}

async function seedUsers() {
  for (const user of users) {
    try {
      const hashedPassword = await bcrypt.hash(user.password, roundsOfHashing);
      await prisma.user.upsert({
        where: { email: user.email },
        update: {},
        create: {
          email: user.email,
          firstname: user.firstname,
          lastname: user.lastname,
          birthdate: new Date(),
          password: hashedPassword,
        },
      });
      console.log(`Added user: ${user.email}`);
    } catch (error) {
      console.error(`Error adding user ${user.email}:`, error);
    }
  }
}
async function seedBottleFormats() {
  for (const bottleFormat of bottleFormats) {
    try {
      await prisma.bottleFormat.upsert({
        where: { name: bottleFormat.name },
        update: {},
        create: bottleFormat,
      });
      console.log(`Added bottle format: ${bottleFormat.name}`);
    } catch (error) {
      console.error(`Error adding bottle format ${bottleFormat.name}:`, error);
    }
  }
}

async function seedWines() {
  for (const wine of wines) {
    try {
      // Trouver le propriétaire et la couleur du vin
      const owner = await prisma.user.findUnique({
        where: { email: wine.email },
      });
      const wineColor = await prisma.wineColor.findUnique({
        where: { name: wine.color },
      });

      // Créer des entrées pour les bouteilles de vin
      const bottleEntries = await Promise.all(
        wine.wineBottles.map(async (bottle) => {
          const format = await prisma.bottleFormat.findUnique({
            where: { name: bottle.formatName },
          });
          return {
            formatId: format.id,
            quantity: bottle.quantity,
            price: bottle.price,
          };
        }),
      );

      // Créer le vin
      await prisma.wine.create({
        data: {
          name: wine.name,
          producer: wine.producer,
          varietal: { set: wine.varietal },
          country: wine.country,
          region: wine.region,
          vintage: wine.vintage,
          description: wine.description,
          servingTemperature: wine.servingTemperature,
          ownerId: owner.id,
          wineColorId: wineColor.id,
          WineBottle: {
            create: bottleEntries,
          },
        },
      });
      console.log(`Added wine: ${wine.name}`);
    } catch (error) {
      console.error(`Error adding wine ${wine.name}:`, error);
    }
  }
}

async function main() {
  const args = process.argv.slice(2);

  if (args.includes('wineColors')) {
    await seedWineColors();
  }
  if (args.includes('users')) {
    await seedUsers();
  }
  if (args.includes('wines')) {
    await seedWines();
  }
  if (args.includes('bottleFormats')) {
    await seedBottleFormats();
  }
  if (!args.length) {
    await seedWineColors();
    await seedBottleFormats();
    await seedUsers();
    await seedWines();
  }
}

main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
