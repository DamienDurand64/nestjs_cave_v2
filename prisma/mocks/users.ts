const PASSWORDS = JSON.parse(process.env.PASSWORD_SECRET);

export const users = [
  {
    email: 'test@test.com',
    password: PASSWORDS[0],
    firstname: 'Test',
    lastname: 'User',
  },
  {
    email: 'joe@dave.com',
    password: PASSWORDS[1],
    firstname: 'Joe',
    lastname: 'Dave',
  },
];
