-- CreateTable
CREATE TABLE "User" (
    "id" SERIAL NOT NULL,
    "firstname" TEXT NOT NULL,
    "lastname" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "birthdate" TIMESTAMP(3) NOT NULL,
    "image" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "User_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "BottleFormat" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "capacity" TEXT NOT NULL,

    CONSTRAINT "BottleFormat_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "WineColor" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "image" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "WineColor_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "TastingNote" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "TastingNote_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Wine" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "producer" TEXT NOT NULL,
    "varietal" TEXT[],
    "country" TEXT NOT NULL,
    "region" TEXT NOT NULL,
    "vintage" INTEGER NOT NULL,
    "purchasedAt" TIMESTAMP(3),
    "consumedAt" TIMESTAMP(3),
    "consumed" BOOLEAN NOT NULL DEFAULT false,
    "description" TEXT,
    "image" TEXT,
    "servingTemperature" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "ownerId" INTEGER NOT NULL,
    "wineColorId" INTEGER NOT NULL,

    CONSTRAINT "Wine_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "WineDrunk" (
    "id" SERIAL NOT NULL,
    "wineId" INTEGER NOT NULL,
    "userId" INTEGER NOT NULL,
    "comment" TEXT NOT NULL,
    "date" TIMESTAMP(3) NOT NULL,
    "format" TEXT NOT NULL,
    "location" TEXT NOT NULL,
    "with" TEXT NOT NULL,
    "quantity" INTEGER NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "WineDrunk_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "WineBottle" (
    "id" SERIAL NOT NULL,
    "wineId" INTEGER NOT NULL,
    "formatId" INTEGER NOT NULL,
    "quantity" INTEGER NOT NULL,
    "price" DOUBLE PRECISION NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "WineBottle_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "_TastingNoteToWine" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "User_email_key" ON "User"("email");

-- CreateIndex
CREATE UNIQUE INDEX "BottleFormat_name_key" ON "BottleFormat"("name");

-- CreateIndex
CREATE UNIQUE INDEX "WineColor_name_key" ON "WineColor"("name");

-- CreateIndex
CREATE UNIQUE INDEX "TastingNote_name_key" ON "TastingNote"("name");

-- CreateIndex
CREATE UNIQUE INDEX "_TastingNoteToWine_AB_unique" ON "_TastingNoteToWine"("A", "B");

-- CreateIndex
CREATE INDEX "_TastingNoteToWine_B_index" ON "_TastingNoteToWine"("B");

-- AddForeignKey
ALTER TABLE "Wine" ADD CONSTRAINT "Wine_ownerId_fkey" FOREIGN KEY ("ownerId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Wine" ADD CONSTRAINT "Wine_wineColorId_fkey" FOREIGN KEY ("wineColorId") REFERENCES "WineColor"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "WineDrunk" ADD CONSTRAINT "WineDrunk_wineId_fkey" FOREIGN KEY ("wineId") REFERENCES "Wine"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "WineDrunk" ADD CONSTRAINT "WineDrunk_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "WineBottle" ADD CONSTRAINT "WineBottle_formatId_fkey" FOREIGN KEY ("formatId") REFERENCES "BottleFormat"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "WineBottle" ADD CONSTRAINT "WineBottle_wineId_fkey" FOREIGN KEY ("wineId") REFERENCES "Wine"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_TastingNoteToWine" ADD CONSTRAINT "_TastingNoteToWine_A_fkey" FOREIGN KEY ("A") REFERENCES "TastingNote"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_TastingNoteToWine" ADD CONSTRAINT "_TastingNoteToWine_B_fkey" FOREIGN KEY ("B") REFERENCES "Wine"("id") ON DELETE CASCADE ON UPDATE CASCADE;
